var pedidos;
pedidos = (function($, undefined) {
   "use strict";
   var events, suscribeEvents, catchDom,dom, fn, initialize, st;
   st = {
   	pedidos : "json/pedidos.json"   
   };
   dom = {};   
   catchDom = function(){
      
   };
   suscribeEvents = function () {      
      
   };
   events = {
      
   };
   fn = {
      cabeceraFixed: function () {
         // body...  
      },
      cargarData : function () {
      	$.ajax({
				type: 'POST',
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				url: st.pedidos,
				data: {},
				timeout: 30000,
				cache: false,
				async: false,
            crossDomain: true,
				beforeSend: function(){
				},
				success: function (oResult) {
					
					$.each(oResult , function (i,el) {
						console.log(oResult[0])
					})
				},
				error: function () {
					console.log("Problems")
				}
			})
      }
      
   };
   initialize = function() {
      catchDom();      
      suscribeEvents();
      fn.cargarData()
   };
   return {
      init: initialize
   };
})(jQuery);

$(function() {
   pedidos.init();   
});